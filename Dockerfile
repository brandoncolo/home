# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM docker.neclatam-cloud.com/node-builder:14 as build-stage

WORKDIR /app
ARG NPM_TOKEN
COPY package.json /app/
RUN echo "Oh dang look at that $NPM_TOKEN"

RUN npm install
RUN npm audit fix --registry=https://registry.npmjs.org
COPY ./ /app/

ARG configuration=production

RUN npm run build -- --output-path=./dist/out --configuration $configuration


# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.17

COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html

COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf

RUN echo "main_es2015_filename=\"\$(ls /usr/share/nginx/html/main-es2015*.js)\" && \
    envsubst '\$BACKEND_URL \$GROUP_PK_SERVICE ' < \${main_es2015_filename} > main_es2015.tmp && \
    mv main_es2015.tmp \${main_es2015_filename} && \
    main_es5_filename=\"\$(ls /usr/share/nginx/html/main-es5*.js)\" && \
    envsubst '\$BACKEND_URL \$GROUP_PK_SERVICE ' < \${main_es5_filename} > main_es5.tmp && \
    mv main_es5.tmp \${main_es5_filename} && \
    nginx -g 'daemon off;'" > run.sh

ENTRYPOINT ["sh", "run.sh"]

EXPOSE 80